//
//  UserDefaultHelper.swift
//  Pec
//
//  Created by Carolain Lenes on 3/06/20.
//  Copyright © 2021 Carolain Lenes All rights reserved.
//

import Foundation

struct UserDefaultHelper {

    static func saveUserDefault(_ objectName: String, _ stringValue: String?, _ booleanValue: Bool?) {
        let userDefault = UserDefaults.standard
        if stringValue != nil {
            userDefault.set(stringValue, forKey: objectName)
        } else {
            userDefault.set(booleanValue, forKey: objectName)
        }
    }
    static func getUserDefault(_ objectName: String) -> (stringValue: String?, booleanValue: Bool?) {
        let userDefault = UserDefaults.standard
        return (userDefault.object(forKey: objectName) as? String, nil)
    }

}
