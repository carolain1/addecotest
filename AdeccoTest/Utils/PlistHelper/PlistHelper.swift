//
//  PlistHelper.swift
//  Pec
//
//  Created by Carolain Lenes on 9/07/20.
//  Copyright © 2021 Carolain Lenes All rights reserved.
//

import Foundation

struct PlistHelper {

    static func getInfoPlistKey(key: String) -> String {
        return (Bundle.main.infoDictionary?[key] as? String)?
            .replacingOccurrences(of: "\\", with: "") ?? ""
    }
}
