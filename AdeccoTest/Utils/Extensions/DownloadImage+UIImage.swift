//
//  DownloadImage+UIImage.swift
//  OrganicNails
//
//  Created by Carolain Lenes on 8/10/20.
//  Copyright © 2020 Simpleapp S.A.S. All rights reserved.
//

import Foundation
import UIKit
import AlamofireImage

extension UIImageView {
    public func imageFromUrl(urlString: URL, width: CGFloat, height: CGFloat) {
        self.af.setImage(withURL: urlString, placeholderImage: #imageLiteral(resourceName: "no_photo.jpeg"), filter: nil, imageTransition: .crossDissolve(0.5), completion: { (response) -> Void in
            guard response.value != nil else {
                return
            }
            let imageResponse = response.value
            DispatchQueue.main.async {
                self.contentMode = .scaleAspectFit
                self.clipsToBounds = true
                self.image = imageResponse
            }
        })
    }
}
