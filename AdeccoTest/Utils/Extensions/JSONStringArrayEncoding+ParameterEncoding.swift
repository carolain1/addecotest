//
//  JSONStringArrayEncoding+ParameterEncoding.swift
//  OrganicNails
//
//  Created by Carolain Lenes on 31/12/20.
//  Copyright © 2021 Carolain Lenes All rights reserved.
//

import Alamofire
import UIKit

struct JSONStringArrayEncoding: ParameterEncoding {
    private let myString: String

    init(string: String) {
        self.myString = string
    }

    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var urlRequest = urlRequest.urlRequest

        let data = myString.data(using: .utf8)!

        if urlRequest?.value(forHTTPHeaderField: "Content-Type") == nil {
            urlRequest?.setValue("text/plain", forHTTPHeaderField: "Content-Type")
        }

        urlRequest?.httpBody = data

        return urlRequest!
    }

}
