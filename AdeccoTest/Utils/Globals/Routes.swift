//
//  Routes.swift
//  Pec
//
//  Created by Carolain Lenes on 3/06/20.
//  Copyright © 2021 Carolain Lenes All rights reserved.
//

import Foundation

public let requestDomain = PlistHelper.getInfoPlistKey(key: "RequestDomain")
public let imageRequest = PlistHelper.getInfoPlistKey(key: "ImageRequest")
public let addecoToken = PlistHelper.getInfoPlistKey(key: "AddecoToken")
