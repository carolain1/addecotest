//
//  Utility.swift
//  AdeccoTest
//
//  Created by Carolain Lenes Beltran on 18/04/21.
//

import Foundation
import MBProgressHUD

open class Utility {
    // Show Load Indicator
    static func showLoading(_ view: UIView) {
        let hud: MBProgressHUD = MBProgressHUD.showAdded(to: view, animated: true)
        hud.bezelView.color = UIColor(named: "blue_dark_color")
        hud.bezelView.backgroundColor = UIColor(named: "white_color")
    }
    
    static func hiddenLoading(_ view: UIView) {
        MBProgressHUD.hide(for: view, animated: true)
    }
}
