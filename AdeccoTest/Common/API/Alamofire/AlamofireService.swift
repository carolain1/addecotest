//
//  AlamofireService.swift
//  Pec
//
//  Created by Carolain Lenes Beltran on 1/06/20.
//  Copyright © 2021 Carolain Lenes All rights reserved.
//

import Foundation
import Alamofire

class AlamofireService {

    static func get(at route: String, params: Parameters = [:]) -> DataRequest {
        return request(at: route, method: .get, params: params, encoding: URLEncoding.default)
    }

    static func post(at route: String, params: Parameters = [:]) -> DataRequest {
        return request(at: route, method: .post, params: params, encoding: JSONEncoding.default)
    }

    static func put(at route: String, params: Parameters = [:]) -> DataRequest {
        return request(at: route, method: .put, params: params, encoding: JSONEncoding.default)
    }

    static func postText(at route: String, params: Parameters = [:], body: String) -> DataRequest {
        return request(at: route, method: .post, params: params, encoding: JSONStringArrayEncoding.init(string: body))
    }

    static func request(at route: String, method: HTTPMethod, params: Parameters = [:], encoding: ParameterEncoding) -> DataRequest {
        let routeUrl = "\(requestDomain)\(route)"
        return AF.request(routeUrl, method: method, parameters: params, encoding: encoding, headers: nil)
            .validate()
    }

    static func getDefaultHeaders() -> HTTPHeaders? {
        if let authToken = UserDefaultHelper.getUserDefault("RequestDomain").stringValue {
            return ["Authorization": "Bearer \(authToken)"]
        } else {
            return nil
        }
    }

}
