//
//  LoginStoresRouter.swift
//  OrganicNails
//
//  Created by Carolain Lenes on 18/04/21.
//  Copyright © 2021 Carolain Lenes All rights reserved.
//

import Foundation

enum MoviesServiceRouter: String {

    case popularMovies
    case movieDetail
    
    var path: String {
        switch self {
        /// Login Router
        case .popularMovies:
            return "movie/popular"
        case .movieDetail:
            return "movie/"
        }
    }
}
