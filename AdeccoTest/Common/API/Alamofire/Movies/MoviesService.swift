//
//  StoresService.swift
//  OrganicNails
//
//  Created by Carolain Lenes on 18/04/21.
//  Copyright © 2021 Carolain Lenes All rights reserved.
//

import Foundation

protocol MoviesService: class {

    typealias PopularMoviesResponse = (Bool, PopularMoviesObject?, Error?) -> Void
    typealias MovieDetailResponse = (Bool, MovieDetail?, Error?) -> Void

    static func getPopularMoviesService(page: Int, completion: @escaping MoviesService.PopularMoviesResponse)
    static func getMovieDetailService(movieId: Int, completion: @escaping MoviesService.MovieDetailResponse)

}
