//
//  AlamofireStoresService.swift
//  OrganicNails
//
//  Created by Carolain Lenes on 18/04/21.
//  Copyright © 2021 Carolain Lenes All rights reserved.
//

import Foundation

class AlamofireMoviesService: AlamofireService, MoviesService {
    
    static func getPopularMoviesService(page: Int, completion: @escaping (Bool, PopularMoviesObject?, Error?) -> Void) {
        let parameters = ["api_key": addecoToken, "page": page] as [String : Any]
        get(at: MoviesServiceRouter.popularMovies.path, params: parameters).responseJSON { (response) in
            print(response)
            switch response.result {
            case .success:
                if response.response?.statusCode != 200 {
                    completion(false, nil, nil)
                } else {
                    do {
                        let dataPopularMovies = try JSONDecoder().decode(APIPopularMovies.self, from: response.data!)
                        let popularMoviesObject = dataPopularMovies.convert()
                        completion(true, popularMoviesObject, nil)
                    } catch let decodingError {
                        completion(false, nil, decodingError)
                    }
                }
            case .failure(let error):
                do{
                    //here dataResponse received from a network request
                    let jsonResponse = try JSONSerialization.jsonObject(with: response.data!, options: [])
                    print(jsonResponse) //Response result
                    completion(false, nil, nil)
                } catch let parsingError {
                    print("Error", parsingError)
                    completion(false, nil, parsingError)
                }
            }
        }
    }
    
    static func getMovieDetailService(movieId: Int, completion: @escaping (Bool, MovieDetail?, Error?) -> Void) {
        let parameters = ["api_key": addecoToken] as [String : Any]
        get(at: "\(MoviesServiceRouter.movieDetail.path)\(movieId)", params: parameters).responseJSON { (response) in
            print(response)
            switch response.result {
            case .success:
                if response.response?.statusCode != 200 {
                    completion(false, nil, nil)
                } else {
                    do {
                        let dataMovieDetail = try JSONDecoder().decode(APIMovieDetail.self, from: response.data!)
                        let MovieDataObject = dataMovieDetail.convert()
                        completion(true, MovieDataObject, nil)
                    } catch let decodingError {
                        completion(false, nil, decodingError)
                    }
                }
            case .failure(let error):
                do{
                    //here dataResponse received from a network request
                    let jsonResponse = try JSONSerialization.jsonObject(with: response.data!, options: [])
                    print(jsonResponse) //Response result
                    completion(false, nil, nil)
                } catch let parsingError {
                    print("Error", parsingError)
                    completion(false, nil, parsingError)
                }
            }
        }
        
    }
    
}
