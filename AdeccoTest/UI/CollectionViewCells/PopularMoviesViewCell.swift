//
//  ProductCollectionViewCell.swift
//  OrganicNails
//
//  Created by Carolain Lenes on 21/09/20.
//  Copyright © 2020 Simpleapp S.A.S. All rights reserved.
//

import UIKit
import AlamofireImage

class PopularMoviesViewCell: UICollectionViewCell {

    //MARK: - IBOutlets -
    @IBOutlet weak var posterImageView: UIImageView!

    func loadPopularMoviesData(dataPropularMoviesRow: Result) {
    
        guard let imageUrl = dataPropularMoviesRow.posterPath else {
            return
        }
        
        let pathString: String = "\(imageRequest)\(imageUrl)"
        
        let url = URL(string: pathString)!

        self.posterImageView.imageFromUrl(urlString: url, width: self.posterImageView.frame.size.width, height: self.posterImageView.frame.size.height)

    }

}
