//
//  MoviesListWireframe.swift
//  AdeccoTest
//
//  Created by Carolain Lenes Beltran on 18/04/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//
//  This file was generated by the 🐍 VIPER generator
//

import UIKit

final class MoviesListWireframe: BaseWireframe {

    // MARK: - Private properties -

    private let storyboard = UIStoryboard(name: "MoviesList", bundle: nil)

    // MARK: - Module setup -

    init() {
        let moduleViewController = storyboard.instantiateViewController(ofType: MoviesListViewController.self)
        super.init(viewController: moduleViewController)

        let interactor = MoviesListInteractor()
        let presenter = MoviesListPresenter(view: moduleViewController, interactor: interactor, wireframe: self)
        moduleViewController.presenter = presenter
    }
    
    init(moduleViewController: MoviesListViewController) {
            super.init(viewController: moduleViewController)

            let interactor = MoviesListInteractor()
            let presenter = MoviesListPresenter(view: moduleViewController, interactor: interactor, wireframe: self)
            moduleViewController.presenter = presenter
        }

}

// MARK: - Extensions -

extension MoviesListWireframe: MoviesListWireframeInterface {
    
    func openMoviewDetailActionWireframe(idMovie: Int) {
        let movieDetailWireframe = MoviesDetailWireframe(idMovie: idMovie)
        self.navigationController?.pushWireframe(movieDetailWireframe, animated: true)
    }
}
