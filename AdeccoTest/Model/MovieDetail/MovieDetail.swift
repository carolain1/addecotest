//
//  MovieDetail.swift
//  AdeccoTest
//
//  Created by Carolain Lenes Beltran on 18/04/21.
//

import Foundation

struct MovieDetail {
    var adult: Bool = false
    var backdropPath: String = ""
    var belongsToCollection: BelongsToCollection = BelongsToCollection()
    var budget: Int = 0
    var genres: [Genre] = []
    var homepage: String = ""
    var id: Int = 0
    var imdbID: String = ""
    var originalLanguage: String = ""
    var originalTitle: String = ""
    var overview: String = ""
    var popularity: Double = 0.0
    var posterPath: String = ""
    var productionCompanies: [ProductionCompany] = []
    var productionCountries: [ProductionCountry] = []
    var releaseDate: String = ""
    var revenue: Int = 0
    var runtime: Int = 0
    var spokenLanguages: [SpokenLanguage] = []
    var status: String = ""
    var tagline: String = ""
    var title: String = ""
    var video: Bool = false
    var voteAverage: Double = 0.0
    var voteCount: Int = 0
}
