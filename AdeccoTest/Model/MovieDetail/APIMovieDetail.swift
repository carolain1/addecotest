//
//  MovieDetail.swift
//  AdeccoTest
//
//  Created by Carolain Lenes Beltran on 18/04/21.
//

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let aPIMovieDetail = try? newJSONDecoder().decode(APIMovieDetail.self, from: jsonData)

import Foundation

// MARK: - APIMovieDetail
struct APIMovieDetail: Codable {
    var adult: Bool?
    var backdropPath: String?
    var belongsToCollection: BelongsToCollection?
    var budget: Int?
    var genres: [Genre]?
    var homepage: String?
    var id: Int?
    var imdbID, originalLanguage, originalTitle, overview: String?
    var popularity: Double?
    var posterPath: String?
    var productionCompanies: [ProductionCompany]?
    var productionCountries: [ProductionCountry]?
    var releaseDate: String?
    var revenue, runtime: Int?
    var spokenLanguages: [SpokenLanguage]?
    var status, tagline, title: String?
    var video: Bool?
    var voteAverage: Double?
    var voteCount: Int?

    enum CodingKeys: String, CodingKey {
        case adult
        case backdropPath = "backdrop_path"
        case belongsToCollection = "belongs_to_collection"
        case budget, genres, homepage, id
        case imdbID = "imdb_id"
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case overview, popularity
        case posterPath = "poster_path"
        case productionCompanies = "production_companies"
        case productionCountries = "production_countries"
        case releaseDate = "release_date"
        case revenue, runtime
        case spokenLanguages = "spoken_languages"
        case status, tagline, title, video
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
        
    }
    
    func convert() -> MovieDetail {
        
        return MovieDetail(adult: adult ?? false, backdropPath: backdropPath ?? "", belongsToCollection: belongsToCollection ?? BelongsToCollection(), budget: budget ?? 0, genres: genres ?? [], homepage: homepage ?? "", id: id ?? 0, imdbID: imdbID ?? "", originalLanguage: originalLanguage ?? "", originalTitle: originalTitle ?? "", overview: overview ?? "", popularity: popularity ?? 0.0, posterPath: posterPath ?? "", productionCompanies: productionCompanies ?? [], productionCountries: productionCountries ?? [], releaseDate: releaseDate ?? "", revenue: revenue ?? 0, runtime: runtime ?? 0, spokenLanguages: spokenLanguages ?? [], status: status ?? "", tagline: tagline ?? "", title: title ?? "", video: video ?? false, voteAverage: voteAverage ?? 0.0, voteCount: voteCount ?? 0)
    }
}

// MARK: - BelongsToCollection
struct BelongsToCollection: Codable {
    var id: Int?
    var name, posterPath, backdropPath: String?

    enum CodingKeys: String, CodingKey {
        case id, name
        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
    }
}

// MARK: - Genre
struct Genre: Codable {
    var id: Int?
    var name: String?
}

// MARK: - ProductionCompany
struct ProductionCompany: Codable {
    var id: Int?
    var logoPath, name, originCountry: String?

    enum CodingKeys: String, CodingKey {
        case id
        case logoPath = "logo_path"
        case name
        case originCountry = "origin_country"
    }
}

// MARK: - ProductionCountry
struct ProductionCountry: Codable {
    var iso3166_1, name: String?

    enum CodingKeys: String, CodingKey {
        case iso3166_1 = "iso_3166_1"
        case name
    }
}

// MARK: - SpokenLanguage
struct SpokenLanguage: Codable {
    var englishName, iso639_1, name: String?

    enum CodingKeys: String, CodingKey {
        case englishName = "english_name"
        case iso639_1 = "iso_639_1"
        case name
    }
}


