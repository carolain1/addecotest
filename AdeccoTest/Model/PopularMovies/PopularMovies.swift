//
//  PopularMovies.swift
//  AdeccoTest
//
//  Created by Carolain Lenes Beltran on 18/04/21.
//

import Foundation

struct PopularMovies {
    var adult: Bool = false
    var backdropPath: String = ""
    var genreIDS: [Int] = []
    var id: Int = 0
    var originalLanguage: String = ""
    var originalTitle: String = ""
    var overview: String = ""
    var popularity: Double = 0.0
    var posterPath: String = ""
    var releaseDate: String = ""
    var title: String = ""
    var video: Bool = false
    var voteAverage: Double = 0.0
    var voteCount: Int = 0
}
