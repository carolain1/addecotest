//
//  APIPopularMovies.swift
//  AdeccoTest
//
//  Created by Carolain Lenes Beltran on 18/04/21.
//

import Foundation

// MARK: - APIPopularMovies
struct APIPopularMovies: Codable {
    var page: Int = 0
    var results: [Result] = []
    var totalPages: Int = 0
    var totalResults: Int = 0

    enum CodingKeys: String, CodingKey {
        case page, results
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
    
    func convert() -> PopularMoviesObject {
        
        return PopularMoviesObject(page: page, results: results, totalPages: totalPages, totalResults: totalResults)
    }
}

// MARK: - Result
struct Result: Codable {
    var adult: Bool? = false
    var backdropPath: String? = ""
    var genreIDS: [Int] = []
    var id: Int = 0
    var originalLanguage: String? = ""
    var originalTitle: String? = ""
    var overview: String? = ""
    var popularity: Double? = 0.0
    var posterPath: String? = ""
    var releaseDate: String? = ""
    var title: String? = ""
    var video: Bool? = false
    var voteAverage: Double? = 0.0
    var voteCount: Int? = 0

    enum CodingKeys: String, CodingKey {
        case adult
        case backdropPath = "backdrop_path"
        case genreIDS = "genre_ids"
        case id
        case originalLanguage = "original_language"
        case originalTitle = "original_title"
        case overview, popularity
        case posterPath = "poster_path"
        case releaseDate = "release_date"
        case title, video
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
    }
    
    func convert() -> PopularMovies {
        
        return PopularMovies(adult: adult ?? false, backdropPath: backdropPath ?? "",
                             genreIDS: genreIDS, id: id, originalLanguage: originalLanguage ?? "",
                             originalTitle: originalTitle ?? "", overview: overview ?? "", popularity: popularity ?? 0.0,
                             posterPath: posterPath ?? "", releaseDate: releaseDate ?? "", title: title ?? "", video: video ?? false,
                             voteAverage: voteAverage ?? 0.0, voteCount: voteCount ?? 0)
    }
}
