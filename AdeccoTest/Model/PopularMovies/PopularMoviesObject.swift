//
//  PopularMoviesObject.swift
//  AdeccoTest
//
//  Created by Carolain Lenes Beltran on 18/04/21.
//

import Foundation

struct PopularMoviesObject {
    var page: Int = 0
    var results: [Result] = []
    var totalPages:Int = 0
    var totalResults: Int = 0
}
